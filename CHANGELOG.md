Change log for Abivia\NextForm-Laravel
====

1.0.2
---
- Change populateViewErrors() to treat a missing bag as empty.

1.0.1
---
- Add bag modifier to populateViewErrors() to allow a named error bag.

1.0.0
---
- Start release tracking
