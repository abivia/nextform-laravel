<?php

/**
 * Mock Laravel's ServiceProvider
 */
namespace Illuminate\Support;

require_once __DIR__ . '/MockBase.php';

class ServiceProvider
{
    use \NextFormLaravelTests\MockBase;

    public $app;

    public function __construct()
    {
        $this->app = new \NextFormLaravelTests\App();
    }
}
