<?php

/**
 * Mock the main NextForm class so we can just test the Laravel extensions.
 */
namespace Abivia\NextForm;

require_once __DIR__ . '/MockBase.php';

/**
 * Mock the native NextForm
 */
class NextForm
{
    use \NextFormLaravelTests\MockBase;

    public function __construct()
    {

    }

    public function addForm($form, $options = [])
    {
        self::_MockBase_log('addForm', [$form, $options]);
        return $this;
    }

    public function addSchema($schema)
    {
        self::_MockBase_log('addSchema', [$schema]);
        return $this;
    }

}
