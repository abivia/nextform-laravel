<?php

namespace NextFormLaravelTests;

require_once 'MockApp.php';
require_once 'MockServiceProvider.php';
//require_once 'MockNextForm.php';

use Abivia\NextFormLaravel\NextFormServiceProvider;
use NextFormLaravelTests\App;
use PHPUnit\Framework\TestCase;

/**
 * @covers Abivia\NextFormLaravel\NextFormServiceProvider
 */
class NextFormServiceProviderTest extends TestCase
{
    public function testBoot() {
        $obj = new NextFormServiceProvider();
        $obj->boot();
        $this->assertTrue(true);
    }

    public function testProvides() {
        $obj = new NextFormServiceProvider();
        $result = $obj->provides();
        $this->assertEquals(['Abivia\NextFormLaravel\NextForm'], $result);
    }

    public function testRegister() {
        $obj = new NextFormServiceProvider();
        $obj->register();
        $log = App::_MockBase_getLog();
        $this->assertEquals('singleton', $log[0][0]);
        $this->assertEquals('nextform', $log[0][1][0]);
        $this->assertInstanceOf('Closure', $log[0][1][1]);
    }

}