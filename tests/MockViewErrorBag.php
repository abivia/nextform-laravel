<?php

/**
 *
 */
namespace Illuminate\Support;

require_once('MockMessageBag.php');

class ViewErrorBag
{
    static public $sample = [
        'seg_key1' => ['message one', 'message two'],
        'seg_key2' => ['message three', 'message four'],
        'key3' => ['message five', 'message six'],
    ];

    public function first($key)
    {
        $keyPlane = self::$sample[$key];
        return $keyPlane[0];
    }

    public function get($key)
    {
        return self::$sample[$key];
    }

    public function getBag($dummy) {
        return new MessageBag();
    }

    public function hasBag($dummy) {
        return $dummy != 'false';
    }

    public function keys()
    {
        return array_keys(self::$sample);
    }

}
