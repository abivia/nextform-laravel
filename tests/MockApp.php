<?php

namespace NextFormLaravelTests;

/**
 * Mock Laravel's ServiceProvider and a mock App
 */

require_once __DIR__ . '/MockBase.php';

class App
{
    use MockBase;

    public function singleton(...$stuff)
    {
        self::_MockBase_log('singleton', $stuff);
    }
}
