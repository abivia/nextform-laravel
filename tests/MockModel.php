<?php

/**
 *
 */

namespace Illuminate\Database\Eloquent;

/**
 * MockModel
 */
class Model
{
    public function getAttributes() {
        $data = ['stuff' => 'bar'];
        return $data;
    }
}
