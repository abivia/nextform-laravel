<?php

/**
 *
 */
namespace Illuminate\Support;

class MessageBag
{
    static public $sample = [
        'seg_key1' => ['message one', 'message two'],
        'seg_key2' => ['message three', 'message four'],
        'key3' => ['message five', 'message six'],
    ];

    public function first($key)
    {
        $keyPlane = self::$sample[$key];
        return $keyPlane[0];
    }

    public function get($key)
    {
        return self::$sample[$key];
    }

    public function keys()
    {
        return array_keys(self::$sample);
    }

}
