<?php

namespace NextFormLaravelTests;

require_once 'mock_functions.php';
require_once 'MockNativeForm.php';
require_once 'MockModel.php';
require_once 'MockViewErrorBag.php';

use Abivia\NextFormLaravel\NextForm;
use PHPUnit\Framework\TestCase;
use Illuminate\Contracts\Translation\Translator as Translator;

/**
 * A  null translator
 */
class NextFormLaravel_MockTranslate implements Translator {

    public function get($key, array $replace = [], $locale = null)
    {
        return $key;
    }

    public function choice($key, $number, array $replace = [], $locale = null)
    {
        return $key;
    }

    public function getLocale()
    {
        return 'no-CA';
    }

    public function setLocale($locale)
    {
    }

}

/**
 * @covers Abivia\NextFormLaravel\NextForm
 */
class NextFormTest extends TestCase
{
    protected $nextForm;
    protected $trans;

    public function setUp() : void
    {
        $this->trans = new NextFormLaravel_MockTranslate();
        $this->nextForm = new NextForm($this->trans);

    }

    public function testInstantiation() {
        NextForm::_MockBase_reset();
        $trans = new NextFormLaravel_MockTranslate();
        $nextForm = new NextForm($trans);
        $this->assertInstanceOf(NextForm::class, $nextForm);
        $log = NextForm::_MockBase_getLog();
        $this->assertEquals(
            [
                ['wireToInstance', ['Translate', $trans]]
            ],
            $log
        );
    }

    public function testAddForm()
    {
        NextForm::_MockBase_reset();
        $someOpts = ['key' => 'value'];
        $this->nextForm->addForm(__FILE__, $someOpts);
        $foo = NextForm::_MockBase_getLog();
        $this->assertEquals(
            [
                [
                    //Addform method
                    'addForm',
                    //parameters to addform (2)
                    [
                        // The processed form list
                        [
                            // First form in the list, with options
                            [__FILE__, $someOpts]
                        ],
                        // null options
                        []
                    ]
                ]
            ],
            NextForm::_MockBase_getLog()
        );
        $this->expectException('\RuntimeException');
        $this->nextForm->addForm('some-non-existent-file.nogo');
    }

    public function testAddFormObj()
    {
        NextForm::_MockBase_reset();
        $obj = new \stdClass();
        $someOpts = ['key' => 'value'];
        $this->nextForm->addForm($obj, $someOpts);
        $foo = NextForm::_MockBase_getLog();
        $this->assertEquals(
            [
                [
                    //Addform method
                    'addForm',
                    //parameters to addform (2)
                    [
                        // The processed form list
                        [
                            // First form in the list, with options
                            [$obj, $someOpts]
                        ],
                        // null options
                        []
                    ]
                ]
            ],
            NextForm::_MockBase_getLog()
        );
    }

    public function testAddSchema()
    {
        NextForm::_MockBase_reset();
        $this->nextForm->addSchema(__FILE__);
        $this->assertEquals(
            [
                ['addSchema', [[__FILE__]]]
            ],
            NextForm::_MockBase_getLog()
        );
    }

    public function testAddSchemaObj()
    {
        NextForm::_MockBase_reset();
        $obj = new \stdClass();
        $this->nextForm->addSchema($obj);
        $this->assertEquals(
            [
                ['addSchema', [[$obj]]]
            ],
            NextForm::_MockBase_getLog()
        );
    }

    public function testCsrfToken()
    {
        NextForm::_MockBase_reset();
        $this->assertEquals(['_token', 'foo'], NextForm::csrfToken());

    }

    public function testPopulate()
    {
        NextForm::_MockBase_reset();
        $data = ['stuff' => 'bar'];
        $this->nextForm->populate($data, 'someseg');
        $this->assertEquals(
            [
                ['populate', [$data, 'someseg']]
            ],
            NextForm::_MockBase_getLog()
        );
    }

    public function testPopulateModel()
    {
        NextForm::_MockBase_reset();
        $data = ['stuff' => 'bar'];
        $model = new \Illuminate\Database\Eloquent\Model;
        $this->nextForm->populate($model, 'someseg');
        $this->assertEquals(
            [
                ['populate', [$data, 'someseg']]
            ],
            NextForm::_MockBase_getLog()
        );
    }

    public function testPopulateViewErrorsDefault()
    {
        NextForm::_MockBase_reset();
        $bag = new \Illuminate\Support\ViewErrorBag();
        $this->nextForm->populateViewErrors($bag);
        $expect = [
            '' => $bag::$sample
        ];
        $this->assertEquals(
            [['populateErrors', [$expect]]],
            NextForm::_MockBase_getLog()
        );
    }

    public function testPopulateViewErrorsFirst()
    {
        NextForm::_MockBase_reset();
        $bag = new \Illuminate\Support\ViewErrorBag();
        $this->nextForm->populateViewErrors($bag, 'first');
        $expect = [
            '' => [
                'seg_key1' => 'message one',
                'seg_key2' => 'message three',
                'key3' => 'message five',
            ]
        ];
        $this->assertEquals(
            [['populateErrors', [$expect]]],
            NextForm::_MockBase_getLog()
        );
    }

    public function testPopulateViewErrorsSegment()
    {
        NextForm::_MockBase_reset();
        $bag = new \Illuminate\Support\ViewErrorBag();
        $this->nextForm->populateViewErrors($bag, 'segments:seg');
        $expect = [
            '' => [
                'key3' => $bag::$sample['key3'],
            ],
            'seg' => [
                'key1' => $bag::$sample['seg_key1'],
                'key2' => $bag::$sample['seg_key2'],
            ]
        ];
        $this->assertEquals(
            [['populateErrors', [$expect]]],
            NextForm::_MockBase_getLog()
        );
    }

    public function testPopulateViewErrorsSegmentFirst()
    {
        NextForm::_MockBase_reset();
        $bag = new \Illuminate\Support\ViewErrorBag();
        $this->nextForm->populateViewErrors($bag, 'first|segments:seg');
        $expect = [
            '' => [
                'key3' => 'message five',
            ],
            'seg' => [
                'key1' => 'message one',
                'key2' => 'message three',
            ]
        ];
        $this->assertEquals(
            [['populateErrors', [$expect]]],
            NextForm::_MockBase_getLog()
        );
    }

}
