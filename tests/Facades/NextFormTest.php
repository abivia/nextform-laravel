<?php

namespace NextFormLaravelTests\Facades;

use Abivia\NextFormLaravel\Facades\NextForm;
use PHPUnit\Framework\TestCase;

/**
 * @covers Abivia\NextFormLaravel\Facades\NextForm
 */
class NextFormTest extends TestCase
{
    public function testAccessor() {
        $this->assertEquals('nextform', NextForm::getFacadeAccessor());
    }
}