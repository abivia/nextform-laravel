<?php

namespace Abivia\NextFormLaravel;

/**
 * Mock our NextForm
 */

require_once __DIR__ . '/MockBase.php';

class NextForm
{
    use \MockBase;

}
