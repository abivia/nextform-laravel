<?php

namespace Abivia\NextFormLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class NextForm extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    public static function getFacadeAccessor()
    {
        return 'nextform';
    }

}
