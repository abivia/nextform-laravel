<?php

namespace Abivia\NextFormLaravel;

use Illuminate\Support\ServiceProvider;
use Abivia\NextFormLaravel\NextForm;

class NextFormServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function boot()
    {
        NextForm::csrfGenerator(NextForm::class . '::csrfToken');
        NextForm::boot();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [NextForm::class];
    }

    /**
     * Register the service provider into the app service container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('nextform', function($app) {
            // Configure the static NextForm DI
            NextForm::wireStatic(
                [
                    'Captcha' => config('view.nextform.captcha')
                        ?? '\Abivia\\NextForm\\Captcha\\SimpleCaptcha',
                    'Render' => config('view.nextform.render')
                        ?? '\Abivia\\NextForm\\Render\\Bootstrap4',
                ]
            );

            // Make confirmation fields work with Laravel validation
            NextForm::$confirmLabel = '_confirmation';

            $nextForm = new NextForm($app->make('translator'));

            return $nextForm;
        });
    }

}
