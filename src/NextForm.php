<?php

declare(strict_types=1);

namespace Abivia\NextFormLaravel;

use Abivia\NextForm\LinkedForm;
use Abivia\NextForm\NextFormException;
use Abivia\NextForm\Render\Block;
use Abivia\NextForm\NextForm as NativeNextForm;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Database\Eloquent\Model;

/**
 * NextFormLaravel
 */
class NextForm Extends NativeNextForm
{

    public function __construct(Translator $translator) {
        parent::__construct();
        $this->wireToInstance('Translate', $translator);
    }

    public function addForm($forms, $options = [])
    {
        if (!is_array($forms)) {
            $forms = [[$forms, $options]];
        }
        foreach ($forms as &$form) {
            if (is_array($form) && count($form) === 1) {
                $form[] = [];
            }
            if (is_string($form[0])) {
                $form[0] = $this->checkResource($form[0]);
            }
        }
        parent::addForm($forms);

        return $this;
    }

    public function addSchema($schemas)
    {
        if (!is_array($schemas)) {
            $schemas = [$schemas];
        }
        foreach ($schemas as &$schema) {
            if (is_string($schema)) {
                $schema = $this->checkResource($schema);
            }
        }
        parent::addSchema($schemas);

        return $this;
    }

    public static function checkResource(string $path) {
        if (!\file_exists($path)) {
            $original = $path;
            $path = resource_path('nextform') . '/' . $path;
            if (!\file_exists($path)) {
                $path .= '.json';
            }
            if (!\file_exists($path)) {
                throw new \RuntimeException(
                    "NextForm can't find the resource \"$original\""
                );
            }
        }
        return $path;
    }

    static function csrfToken()
    {
        return ['_token', csrf_token()];
    }

    /**
     * Call the native generate() and then save any context in the session.
     *
     * @param mixed $oneForm
     * @param array $options
     *
     * @return \Abivia\NextFormLaravel\Block
     */
    public function generate($oneForm = null, $options = []) : Block
    {
        $block = parent::generate($oneForm, $options);
        session(['NextForm' => $this->getContext()]);
        return $block;
    }

    /**
     * Get a validation object for a set of fields.
     *
     * @param array The form data (eg. from $request->all()).
     * @param array|string $validate Segment name or array of
     * [segmentName][properties].
     * @param array|null Field list (when $validate is a string).
     *
     * @return Validator
     */
    public static function getValidations($input, $validate, $fields = null)
    : Validator {
        if ($fields !== null) {
            $validate = [$validate => $fields];
        }
        foreach ($validate as $segName => $props) {
            $segment = app('nextform')->getSegment($segName);
            foreach ($props as $prop) {
                $rules["{$segName}_{$prop}"] = $segment->getProperty($prop)
                    ->getValidation()->get('rules');
            }
        }

        return $rules;
    }


    public function populate($data, ?string $segment = '')
    {
        if ($data instanceof Model) {
            parent::populate($data->getAttributes(), $segment);
        } else {
            parent::populate($data, $segment);
        }
        return $this;
    }

    /**
     * Extract errors from a Laravel error bag and pass to NextForm.
     * @param ViewErrorBag $viewErrors
     * @param string|null $modifiers delimited by |
     * bag:name -> take messages from the named bag
     * first -> just take the first error in the bag, otherwise take them all;
     * segments:name1,name2... -> Extract messages for the segment(s) and
     * store using the field name.
     */
    public function populateViewErrors(
        ViewErrorBag $viewErrors,
        ?string $modifiers = ''
    ) {
        $first = false;
        $segments = [];
        $bag = 'default';
        foreach (\explode('|', $modifiers) as $clause) {
            $parts = \explode(':', $clause);
            if ($parts[0] === 'first') {
                $first = true;
            } elseif ($parts[0] === 'bag') {
                if (!isset($parts[1])) {
                    continue;
                }
                $bag = trim($parts[1]);
                if (!$viewErrors->hasBag($bag)) {
                    return;
                }
            } elseif ($parts[0] === 'segments') {
                if (!isset($parts[1])) {
                    continue;
                }
                foreach (explode(',', $parts[1]) as $segName) {
                    $segName = trim($segName);
                    $segments[$segName] = [$segName . '_', strlen($segName) + 1];
                }
            }
        }
        $messageBag = $viewErrors->getBag($bag);
        $messages = [];
        foreach ($messageBag->keys() as $key) {
            $segment = '';
            $field = $key;
            foreach ($segments as $name => $prefix) {
                if (Str::startsWith($key, $prefix[0])){
                    $segment = $name;
                    $field = substr($key, $prefix[1]);
                }
            }
            $messages[$segment] ??= [];
            if ($first) {
                $messages[$segment][$field] = $messageBag->first($key);
            } else {
                $messages[$segment][$field] = $messageBag->get($key);
            }
        }
        $this->populateErrors($messages);
    }

}
