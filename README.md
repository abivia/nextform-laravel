This package integrates NextForm capabilities into a Laravel application.

For a example use in an application, visit
https://gitlab.com/abivia/nextform-laravel-demo

The package looks for schema and form definitions in `resources/nextform`.

The default render engine is Bootstrap4. This can be explicitly set by adding
these settings to `config/view.php`:

```php
    'nextform' => [
        'renderer' => 'Bootstrap4'
    ],
```
Example of loading a schema and populating the data in a controller:

```php
    public function edit($id)
    {
        $test = Test::findOrFail($id);
        NextForm::addSchema('test-schema');
        NextForm::populate($test);

        return view('tests/edit', compact('test'));
    }
```

NextForm generates the HTML for the form, but also includes links to required
CSS and JS libraries, as well as any form-specific JS. A skeletal layout file
might look like this:

```
layout/blade.php

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>NextForm Demo</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
  @yield('pagelinks')
</head>
<body>
  <div class="container">
    @yield('content')
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
  @yield('script')
</body>
</html>
```

The view for the form might look like this:

```
edit.blade.php

@extends('../layout')
<?php
NextForm::generate(
    'test-form',
    [
        'action' => route('tests.update', $test->id),
        'state' => ['_method' => 'PATCH']
    ]
);
?>
@section('content')
    @lang('Update Tests')
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <?php NextForm::body(); ?>
@endsection
@section('pagelinks')
<?php NextForm::links(); ?>
@endsection
@section('script')
<?php NextForm::scriptFiles(); ?>
<script>
<?php NextForm::script(); ?>
</script>
@endsection
```
